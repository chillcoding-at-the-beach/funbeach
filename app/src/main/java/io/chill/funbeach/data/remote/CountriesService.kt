package io.chill.com.data.remote

import io.chill.com.data.Country
import retrofit2.Call
import retrofit2.http.GET

interface CountriesService {
    //https://restcountries.com/v3.1/all
    @GET("/v3.1/all")
    fun listCountries(): Call<List<Country>>
}