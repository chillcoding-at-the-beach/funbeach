package io.chill.funbeach

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast

/**
 * The first class to code in an Android project.
 * Just have fun with Android app. lifecycle, in particular
 * the Activity have 5 states: onCreate, onResume, onPause, onStop, OnDestroy.
 * The basics is to play with Log, override Activity methods, see effects in Logcat console
 * according to user actions (open the app. change to an other app. receive a call, etc.).
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, R.string.text_hello, Toast.LENGTH_LONG).show()
        Log.i(MainActivity::class.simpleName, "####Hello Logcat!!!")
    }

    override fun onResume() {
        super.onResume()
        Log.i(MainActivity::class.simpleName, "####Hello")
    }
}