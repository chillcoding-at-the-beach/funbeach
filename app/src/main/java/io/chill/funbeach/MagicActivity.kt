package io.chill.funbeach

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

/**
 * First step of tuto. on Kotlin language (cf. Kotlin part in "Kotlin pour Android : quiz").
 * Just have fun with MagicCircle.
 */
class MagicActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_magic)
    }
}