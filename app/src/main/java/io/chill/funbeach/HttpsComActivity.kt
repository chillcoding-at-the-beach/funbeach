package io.chill.funbeach

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import io.chill.com.data.Country
import io.chill.com.data.remote.CountriesService
import io.chill.funbeach.beach.ui.theme.FunBeachTheme
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * This class to showcase a basic HTTPS communication according to CHILLcoding tuto (cf. https://www.chillcoding.com/android-retrofit-send-http/).
 */
class HttpsComActivity : ComponentActivity() {

    companion object {
        const val URL_COUNTRY_API = "https://restcountries.com/"
    }

    private lateinit var countryRequest: Call<List<Country>>

    var httpsComList: List<Country> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Be sure to instantiate countryRequest, because of lateinit!
        instantiateRetro()
        sendRequest()
    }

    fun instantiateRetro() {
        val retro = Retrofit.Builder().baseUrl(URL_COUNTRY_API)
            .addConverterFactory(MoshiConverterFactory.create()).build()
        val service = retro.create(CountriesService::class.java)
        countryRequest = service.listCountries()
    }

    fun sendRequest() {
        countryRequest.enqueue(object : Callback<List<Country>> {
            override fun onResponse(call: Call<List<Country>>, response: Response<List<Country>>) {
                val countryList = response.body()
                if (countryList != null) {
                    httpsComList = countryList
                    updateUI()
                    // V0: show the result in Logcat
//                    for (country in countryList) Log.v(
//                        HttpsComActivity::class.simpleName,
//                        "COUNTRY ${country.name.common}"
//                    )
                }
            }

            override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                Log.i(HttpsComActivity::class.simpleName, "RETRY AND GOOD LUCK")
            }
        })
    }

    /**
     * Fake architecture to simply show the result in the app instead of log.
     * This function is called after the request.
     * In a real app. component architecture and livedata are used.
     */
    fun updateUI(){
        setContent {
            FunBeachTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    CountryList(countryList = httpsComList)
                }
            }
        }
    }
}

@Composable
fun CountryList(countryList: List<Country>) {
    // LazyColumn to display a simple list with
    // compose (cf. https://developer.android.com/jetpack/compose/lists)
    LazyColumn(modifier = Modifier.padding(vertical = 4.dp)) {
        for (c in countryList) {
            item {
                Text(c.name.common)
            }
        }
    }
}