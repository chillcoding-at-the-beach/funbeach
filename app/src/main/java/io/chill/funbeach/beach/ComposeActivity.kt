package io.chill.funbeach.beach

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.chill.funbeach.beach.data.Beach
import io.chill.funbeach.beach.ui.theme.FunBeachTheme

/**
 * This compose class to display a list in 10 min.
 * It's an alternative to recyclerView.
 * Nevertheless, dependencies config. in gradle take time to solve compatibility
 * with Kotlin and material libraries.
 */
class ComposeActivity : ComponentActivity() {

    // V0: Fake Data model
    val items = List<Beach>(9, { Beach("Lacanau") })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FunBeachTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    BeachList(beachList = items)
                }
            }
        }
    }
}

/**
 * UI Componant coding with compose lib.
 * .: Return a simple list at the screen
 */
@Composable
fun BeachList(beachList: List<Beach>) {
    // LazyColumn pour afficher une liste avec la biblio.
    // compose (cf. https://developer.android.com/jetpack/compose/lists)
    LazyColumn(modifier = Modifier.padding(vertical = 4.dp)) {
        for (beach in beachList) {
            item {
                Text(beach.name)
            }
        }
    }
}

/* Android Studio Preview with Compose
 * Useful to build a layout with compose box
 */
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    FunBeachTheme {

    }
}