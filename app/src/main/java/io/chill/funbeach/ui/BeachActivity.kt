package io.chill.funbeach.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import io.chill.funbeach.R
import io.chill.funbeach.databinding.ActivityBeachBinding

class BeachActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBeachBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBeachBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /**
         * Version 0 : /!\ les appels à la méthode findViewById sont couteux.
         */
        // val btn = findViewById<Button>(R.id.castleBtn)
        /**
         * Version 1 : Après Mise en place du viewBinding dans le fichier gradle
         * il est plus facile et sûre d'accèder aux éléments de la vue XML
         */
        binding.castleBtn.setOnClickListener {
            binding.castleTxt.text = getString(R.string.text_hello)
            Toast.makeText(this, R.string.text_not_click, Toast.LENGTH_SHORT).show()
        }
    }
}