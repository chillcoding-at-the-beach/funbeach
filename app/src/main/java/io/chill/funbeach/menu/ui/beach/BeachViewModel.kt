package io.chill.funbeach.menu.ui.beach

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.chill.funbeach.R

class BeachViewModel : ViewModel() {

    private val _text = MutableLiveData<Int>().apply {
        value = R.string.title_beach
    }
    val text: LiveData<Int> = _text
}