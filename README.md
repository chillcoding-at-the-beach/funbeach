![OS](https://badgen.net/badge/OS/Android?icon=https://raw.githubusercontent.com/androiddevnotes/awesome-android-kotlin-apps/master/assets/android.svg&color=3ddc84)
![Language](https://img.shields.io/github/languages/top/cortinico/kotlin-android-template?color=blue&logo=kotlin)

# Fun Beach

<img src="https://gitlab.com/chillcoding-at-the-beach/funbeach/raw/main/app/src/main/funBeach-ui.gif" >


This is a sample of popular developments for a mobile Android app.,
Coding during a training session where we talk about:
1. 101Android
2. Kotlin
3. Native User Interface
4. List and RecyclerView
4. Menus
5. Persitence of data
6. HTTPS Communication
7. Publication

# References
[Kotlin for Android: quiz on GitLab](https://gitlab.com/chillcoding-at-the-beach/kotlin-for-android)

[chillcoding.com: Blog](https://www.chillcoding.com/blog/)

[github.com: Now in Android App](https://github.com/android/nowinandroid)

## Authors
 [Macha DA COSTA on LinkedIn](https://www.linkedin.com/in/MachaDaCosta/)

## License
It's an open source projects licensed under a classic GNU licence. [How to choose a license](https://choosealicense.com)


